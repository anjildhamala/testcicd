export class SearchCriteria {
  countryId = 78;
  minImdbVotes: number;
  netflixStartRating: number;
  netflixEndRating: number;
  subtitle: string;
  sortBy: string;
  startYear: number;
  endYear: number;
  startImdbRating: number;
  endImdbRating: number;
  audioType: string;
  genreIds: number;
  videoType: string;
}
