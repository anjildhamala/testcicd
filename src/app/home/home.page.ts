import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SearchCriteria } from './search-criteria';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private http: HttpClient) {}

  searchMovies(search: SearchCriteria): Observable<any> {
    return this.http.get('https://unogs-unogs-v1.p.rapidapi.com/aaapi.cgi?q=' + search.toString());
  }
}
